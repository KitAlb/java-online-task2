package com.epam;

import java.util.Scanner;

public class Rumors {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter number of cases: ");
        int cases = scanner.nextInt();

        System.out.println("Enter how much guests should be at the party (>2): ");
        int guestsNumber = scanner.nextInt();

        int fullSpreadCount = 0;
        int peopleHeardRumor = 0;

        for (int i = 0; i < cases; i++) {

            boolean[] guests = new boolean[guestsNumber];
            guests[1] = true;

            boolean alreadyHeard = false;

            int nextGuestIndex;
            int currentGuestIndex = 1;

            while (!alreadyHeard) {
                nextGuestIndex = getNewGuestIndex(currentGuestIndex, guestsNumber);

                if (guests[nextGuestIndex]) {

                    if (isRumorWidespread(guests)) {
                        fullSpreadCount++;
                    }

                    peopleHeardRumor = peopleHeardRumor + countGuestsHeardRumor(guests);
                    alreadyHeard = true;
                }

                guests[nextGuestIndex] = true;
                currentGuestIndex = nextGuestIndex;
            }
        }

        System.out.println("Empirical probability that everyone will hear rumor except Alice in " + cases + " cases: " +
                (double) fullSpreadCount / cases);
        System.out.println("Average amount of people that rumor reached is: " + peopleHeardRumor / cases);
    }

    private static int getNewGuestIndex(int currentGuestIndex, int guestsNumber) {
        int nextGuestIndex = getRandomGuestIndex(guestsNumber);

        while (nextGuestIndex == currentGuestIndex) {
            nextGuestIndex = getRandomGuestIndex(guestsNumber);
        }

        return nextGuestIndex;
    }

    private static int getRandomGuestIndex(int guestsNumber) {
        return 1 + (int) (Math.random() * (guestsNumber - 1));
    }

    private static int countGuestsHeardRumor(boolean[] guests) {
        int counter = 0;

        for (int i = 1; i < guests.length; i++) {
            if (guests[i]) {
                counter++;
            }
        }

        return counter;
    }

    private static boolean isRumorWidespread(boolean[] guests) {
        for (int i = 1; i < guests.length; i++) {
            if (!guests[i]) {
                return false;
            }
        }

        return true;
    }

}
